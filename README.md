#How to
Create Virtual Env

'''
python3 -m venv env_name
'''
Activate Virtual Env

'''
source env_name/bin/activate
'''
Install pack
'''
pip install -r requirements.txt
'''

